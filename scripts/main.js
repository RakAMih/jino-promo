$(document).ready(function () {
  $('.navi').on('click', 'a', function (event) {
    event.preventDefault();
    var id = $(this).attr('href');
    var position = $(id).position();
    $('body,html').animate({
      scrollTop: position.top
    }, 1000);
  });
  //special for IE11
  if ('NodeList' in window && !NodeList.prototype.forEach) {
    console.info('polyfill for IE11');
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }
  $('#button-check').on('click', checkDomain);
  $('[data-input="check-domain"]').on('keydown', function (e) {
    $('#input-top').removeClass('input-error');

    if (e.keyCode === 13) {
      checkDomain();
    }
  });

  function checkDomain() {
    var inputEl = $('[data-input="check-domain"]');
    var regExpDomain = /^[0-9A-Za-z\u017F\u212A][\x2D0-9A-Za-z\u017F\u212A]{1,61}[0-9A-Za-z\u017F\u212A]\.[A-Za-z\u017F\u212A]{2,}$/ig;
    var val = inputEl.val();

    if (regExpDomain.test(val)) {
      $('[data-id="domain-text"]').text(inputEl.val());
    } else {
      $('#input-top').addClass('input-error');
    }
  }

  var elements = document.querySelectorAll('.list-item');
  elements.forEach(function (item) {
    item.addEventListener('click', function (e) {
      // console.log(e.target);
      var parent = $(e.target).closest('.list-item')[0];
      var el = parent.querySelector('.description');

      if (!el.classList.contains('show')) {
        elements.forEach(function (i) {
          var el = i.querySelector('.description');
          el.classList.remove('show');
        });
        el.classList.add('show');
      }
    });
  });
});